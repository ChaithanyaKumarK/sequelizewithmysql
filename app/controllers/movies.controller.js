const db = require("../models");
const Movie = db.movies;
const Op = db.Sequelize.Op;

function checkInputData(data){
  console.log("data")
  console.log(data)
   if(data.Rank == "undefined" || (/[A-Za-z]/).test(data.Rank) ){
     return true;
   }
   if(data.Title == "undefined" || (/\d/).test(data.Title)){
     return true;
   }
   if(data.Description == "undefined" || (/\d/).test(data.Description)){
     return true;
   }
   if(data.Runtime == "undefined" || (/[A-Za-z]/).test(data.Runtime)){
    return true;
  }
   if(data.Rating == "undefined" || (/[A-Za-z]/).test(data.Rating)){
     return true;
   }
   if(data.Metascore == "undefined" || (/[A-Za-z]/).test(data.Metascore)){
     return true;
   }
   if(data.Votes == "undefined" || (/[A-Za-z]/).test(data.Votes)){
     return true;
   }
   if(data.Actor == "undefined" || (/\d/).test(data.Actor))
   {
     return true;
   }
   if(data.Year == "undefined" || (/[A-Za-z]/).test(data.Year)){
     return true;
   }
   return false;
}

exports.create = (req, res) => {
        if (checkInputData(req.body)) {
          res.status(400).send({
            message: "Invalid input"
          }).end();
          
        }else{
          const movie = {
            rank: req.body.Rank,
            title: req.body.Title,
            description: req.body.Description,
            runtime: req.body.Runtime,
            rating: req.body.Rating,
            metascore: req.body.Metascore,
            votes: req.body.Votes,
            gross_earning_in_mil: req.body.Gross_Earning_in_Mil,
            actor: req.body.Actor,
            year: req.body.Year
           
          };
          console.log("t1");
          Movie.create(movie)
          .then(data => {
            console.log("t2")
            res.status(201).json({
              "msg": "Movie record created successfully"
            });
          })
          .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while creating the movies."
            });
          });
          res.end();
        }     
};

exports.findAll = (req, res) => {

  Movie.findAll()
  .then(data => {
      res.status(200).send(data).end();
  })
  .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Directors."
      }).end();
  });
};

exports.delete = (req, res) => {
  const id = parseInt(req.params.id);

  Movie.destroy({
    where: { id: id }
  })
  .then(num => {
      if (num == 1) {
        res.status(202).json({
          message: "Movie was deleted successfully!"
        }).end();
      } else {
        res.status(400).json({
          message: `Cannot delete Movie with id=${id}. Maybe Movie was not found!`
        }).end();
      }
  })
  .catch(err => {
      res.status(500).send({
        message: "Could not delete Movie with id=" + id
      }).end();
  });
};

exports.update = (req, res) => {
  const id = parseInt(req.params.id);
  if(checkInputData(req.body)){
    res.status(400).send({
      message: "Invalid Input"
    }).end();

  }else{
    movies_object={
      rank : req.body['Rank'],
      title : req.body['Title'],
      description : req.body['Description'],
      runtime : req.body['Runtime'],
      rating : req.body['Rating'],
      metascore : req.body['Metascore'],
      votes : req.body['Votes'],
      gross_earning_in_mil : req.body['Gross_Earning_in_Mil'],
      actor : req.body['Actor']
    };

    Movie.update(movies_object, {
      where: { id: id }
    })
    .then(num => {
      
        if (num == 1) {
          res.status(202).json({
            message: "Movie was updated successfully."
          }).end();
        } else {
          res.status(400).json({
            message: `Cannot update Movie with id=${id}. Maybe Movie was not found or req.body is empty!`
          }).end();
        }
    })
    .catch(err => {
        res.status(500).send({
          message: "Error updating Movie with id=" + id
        }).end();
    });
  }  
};