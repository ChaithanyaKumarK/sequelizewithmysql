const db = require("../models");
const Genres = db.genres;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {

        if (!req.body.Genre || (/\d/).test(req.body.Genre)) {
          res.status(400).send({
            message: "Invalid input"
          }).end();
        }else{
          const genre = {  
            genre: req.body.Genre    
          };
        
          Genres.create(genre)
          .then(data => {
              res.status(201).send(data).end;
          })
          .catch(err => {
              res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Genres."
              });
          });
        }     
        
};

exports.findAll = (req, res) => {
  
        Genres.findAll()
        .then(data => {
            res.status(200).send(data).end();
        })
        .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while retrieving Genres."
            });
        });
};

exports.delete = (req, res) => {
        const id = parseInt(req.params.id);
      
        Genres.destroy({
          where: { id: id }
        })
        .then(num => {
            if (num == 1) {
              res.status(202).send({
                message: "Genres was deleted successfully!"
              }).end();
            } else {
              res.status(400).send({
                message: `Cannot delete Genres with id=${id}. Maybe Genres was not found!`
              }).end();
            }
        })
        .catch(err => {
            res.status(500).send({
              message: "Could not delete Genres with id=" + id
            }).end();
        });
};


exports.update = (req, res) => {
        const id = parseInt(req.params.id);
        if (!req.body.Genre || (/\d/).test(req.body.Genre)) {
          res.status(400).send({
            message: "Invalid input"
          }).end();
        }else{
          const genre = {  
                  genre: req.body.Genre    
          }

          Genres.update(genre, {
            where: { id: id }
          })
          .then(num => {
              if (num == 1) {
                res.send({
                  message: "Genres was updated successfully."
                });
              } else {
                res.send({
                  message: `Cannot update Genres with id=${id}. Maybe Genres was not found or req.body is empty!`
                });
              }
          })
          .catch(err => {
              res.status(500).send({
                message: "Error updating Genres with id=" + id
              });
          });
        }
};