const db = require("../models");
const Director = db.directors;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
       
        if (req.body.Director == 'undefined' || (/\d/).test(req.body.Director) ) {
          res.status(400).send({
            message: "Invalid Input"
          }).end();
        }else{
          const director = {
            director_name: req.body.Director
          };
        
          Director.create(director)
          .then(data => {
          res.status(200).send(data);
          })
          .catch(err => {
                  res.status(500).send({
                          message:
                          err.message || "Some error occurred while creating the Director."
                  }).end();
          });

        }
      
        
};

exports.findAll = (req, res) => {
   
        Director.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while retrieving Directors."
            }).end();
        });
};

exports.delete = (req, res) => {
        const id = parseInt(req.params.id);
      
        Director.destroy({
          where: { id: id }
        })
        .then(num => {
            if (num == 1) {
              res.status(200).send({
                message: "Director was deleted successfully!"
              }).end();
            } else {
              res.status(400).send({
                message: `Cannot delete Director with id=${id}. Maybe Director was not found!`
              }).end();
            }
        })
        .catch(err => {
            res.status(500).send({
              message: "Could not delete Director with id=" + id
            }).end();
        });
};

exports.update = (req, res) => {
        const id = parseInt(req.params.id);
        if (req.body.Director == 'undefined' || (/\d/).test(req.body.Director) ) {
          res.status(400).send({
            message: "Invalid Input"
          }).end();
        }else{
          director_object ={
            director_name: req.body.Director
          }
          console.log(director_object)

          Director.update(director_object, {
            where: { id: id }
          })
          .then(num => {
              if (num == 1) {
                res.status(200).send({
                  message: "Director was updated successfully."
                }).end();
              } else {
                res.status(400).send({
                  message: `Cannot update Director with id=${id}. Maybe Director was not found or req.body is empty!`
                }).end();
              }
          })
          .catch(err => {
              res.status(500).send({
                message: "Error updating Director with id=" + id
              }).end();
          });
        }
};