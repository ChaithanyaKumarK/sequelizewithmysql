const db = require("../models");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Users = db.users;
const Op = db.Sequelize.Op;
require('dotenv').config();

exports.create = async (req, res) => {
        if (req.body.user == 'undefined' || req.body.password == 'undefined') {
          res.status(400).send({
            message: "Content can not be empty!"
          }).end();
        }
      
        try{
                const hashedPassword = await bcrypt.hash(req.body.password,10);
                const user_ob ={
                        'user': req.body.user,
                        'password': hashedPassword
                }
                Users.create(user_ob)
                .then(data => {
                        res.status(201).json({
                                "msg": "user created"
                        }).end();
                })
                .catch(err => {
                        res.status(500).send({
                                message:
                                err.message || "Some error occurred while creating the user."
                        }).end();
                });
        }catch(err) {
                console.log(err);
                res.status(500).send();
        }
      
};


exports.login = async (req, res) => {
        if (req.body.user == 'undefined' || req.body.password == 'undefined') {
          res.status(400).send({
            message: "Content can not be empty!"
          }).end();
        }
        try{
                const usersData = await Users.findAll();
                const user =usersData.find(user_ob=> user_ob.user ==req.body.user);

                if (await bcrypt.compare(req.body.password, user.password)){
                        const accessToken = jwt.sign({user}, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' });
                        res.json({accessToken :accessToken})
                        
                }else{
                        res.json({
                         "msg": 'Not allowed'
                        })
                }
        }catch(err){
                console.log(err);
                res.status(500).send({
                        message:
                          err.message || "Some error occurred while login."
                });
        }
};
