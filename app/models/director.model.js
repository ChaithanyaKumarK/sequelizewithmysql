module.exports = (sequelize, Sequelize) => {
        const Directors = sequelize.define("director", {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
          },
          director_name: {
            type: Sequelize.STRING
          }
        });
      
        return Directors;
};