module.exports = (sequelize, Sequelize) => {
        const Genres = sequelize.define("genres", {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
          },
          genre: {
            type: Sequelize.STRING
          }
        });
      
        return Genres;
};