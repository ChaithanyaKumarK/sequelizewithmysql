module.exports = (sequelize, Sequelize) => {
  const Movies = sequelize.define("movies", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    rank: {
      type: Sequelize.STRING
    },
    title: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    runtime: {
      type: Sequelize.INTEGER
    },
    rating: {
      type: Sequelize.FLOAT
    },
    metascore: {
      type: Sequelize.INTEGER
    },
    votes: {
      type: Sequelize.INTEGER
    },
    gross_earning_in_mil:{
      type: Sequelize.FLOAT
    },
    actor:{
      type: Sequelize.STRING
    }
  });

  return Movies;
}
