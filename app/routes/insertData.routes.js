module.exports = app => {
        const insertData = require("../controllers/insertData.controller.js");
      
        var router = require("express").Router();
      
        router.post("/director", insertData.director);
        router.post("/genres", insertData.genres);
        router.post("/movies", insertData.movies);
    
        app.use('/api/insert', router);
};