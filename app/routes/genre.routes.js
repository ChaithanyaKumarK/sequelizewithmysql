const jwt = require('jsonwebtoken');
module.exports = app => {
        const genre = require("../controllers/genre.controller.js");
      
        var router = require("express").Router();
        router.use(function authenticateToken(req, res, next) {
                const authHeader = req.headers['authorization']
                const token = authHeader && authHeader.split(' ')[1]
                if (token == null) return res.sendStatus(401)
              
                jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
                        console.log(err)
                        if (err) {
                                console.log(err);
                                res.json({
                                        "msg" : "authenticate fail"
                                })
                        }else{
                                next();  
                        }
                        
                        
                })
        })

        router.post("/", genre.create);
      
        router.get("/", genre.findAll);

        router.put("/:id", genre.update);
        
        router.delete("/:id", genre.delete);
     
        app.use('/api/genre', router);
};